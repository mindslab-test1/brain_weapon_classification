'''
    Author: Sungguk Cha
    Weapon gRPC client
'''

### proto
from weapon_pb2 import RequestTrain, RequestLoad, RequestTest
from weapon_pb2_grpc import WeaponServiceStub

### utils
from PIL import Image
from server import *
import argparse
import cv2
import grpc
import io
import numpy as np
import yaml

CHUNK_SIZE = 1024 * 1024 # 1MB

### Wrapper (deprecated)
def ImageWrapper(item):
    ticket = ImageIn()
    ticket.image = item
    return ticket

###
class Client(object):
    def __init__(self, args):
        self.remote = "127.0.0.1:{}".format(args.port)
        self.channel = grpc.insecure_channel(self.remote)
        self.stub = WeaponServiceStub(self.channel)
        self.chunk_size = 1024 * 1024

    def _generate_img_binary_iterator(self, img_binary):
        for idx in range(0, len(img_binary), self.chunk_size):
            yield RequestTest(image=img_binary[idx:idx+self.chunk_size])

    def train(self, conf):
        request_train = RequestTrain(conf_path=conf)
        print(self.stub.Train(request_train).is_success)

    def load(self, checkpoint):
        request_load = RequestLoad(checkpoint_path=checkpoint)
        print(self.stub.Load(request_load).is_success)

    def test(self, img):
        request_test = self._generate_img_binary_iterator(img)
        print(self.stub.Test(request_test).weapon_list)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', default=33633, type=int)
    parser.add_argument('--mode', type=str)
    parser.add_argument('--checkpoint', type=str)
    parser.add_argument('--image-dir', default='./samples/fnx40.png')
    parser.add_argument('--yaml-dir', default='./samples/default.yaml')
    return parser.parse_args()

if __name__ == '__main__':
    args = get_args()
    client = Client(args)

    if args.mode == "train":
        with open (args.yaml_dir) as rf:
            conf = yaml.load(rf)
            conf = yaml.dump(conf)
        client.train(conf)
    elif args.mode == 'load':
        client.load(args.checkpoint)
    elif args.mode == 'test':
        with open(args.image_dir, 'rb') as rf:
            data = rf.read()
        client.test(data)
