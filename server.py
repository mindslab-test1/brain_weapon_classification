'''
    Author: Sungguk Cha
    Weapon gRPC server
'''

### proto
from weapon_pb2 import Response, ResponseTest
from weapon_pb2_grpc import WeaponServiceServicer, add_WeaponServiceServicer_to_server
### Weapon
from weapon import Weapon
### utils
from concurrent import futures
from PIL import Image
import argparse
import grpc
import io
import time
import yaml

CHUNK_SIZE = 1024 * 1024 # 1MB

### LOADING
def bytes2chunks(Bs, message_wrapper):
    for idx in range(0, len(Bs), CHUNK_SIZE):
        yield message_wrapper(Bs[idx:idx+CHUNK_SIZE])

def chunks2bytes(chunks):
    res = bytearray()
    for chunk in chunks:
        res.extend(chunk.image)
    return bytes(res)

### Server
class Weapon_server(WeaponServiceServicer):
    def __init__(self, checkpoint_path):
        self.weapon = Weapon()
        if checkpoint_path != "":
            self.weapon.load(checkpoint_path)

    def Load(self, request, context):
        try:
            self.weapon.load(request.checkpoint_path)
            return Response(is_success=True)
        except Exception as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return Response(is_success=False)

    def Test(self, request_iter, context):
        image = chunks2bytes(request_iter)
        try:
            image = Image.open(io.BytesIO(image))
            image = image.convert('RGB')
            image = image.resize((224, 224), Image.BILINEAR)
            res = self.weapon.test(image)
            return ResponseTest(weapon_list=res)
        except Exception as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))

    def Train(self, request, context):
        conf = yaml.safe_load(request.conf_path)
        class Args:
            pass
        args = Args()
        for k, v in conf.items():
            if k == 'lr': v = float(v)
            setattr(args, k, v)
        try:
            self.weapon.train(args)
            return Response(is_success=True)
        except Exception as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return Response(is_success=False)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', default=33633)
    parser.add_argument('--checkpoint_path', default="")
    return parser.parse_args()

if __name__ == '__main__':
    args = get_args()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    weapon_server = Weapon_server(args.checkpoint_path)
    weapon_servicer = add_WeaponServiceServicer_to_server(weapon_server, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    server.start()

    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)
