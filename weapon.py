r'''
    Author: Sungguk Cha
    image classification script
'''
import weapon_pb2
from dataloader.pistol import *
from models.resnet import *
from torch.utils import data
from torchvision import transforms
from tqdm import tqdm

import argparse
import os
import time
import torch

opj = os.path.join

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)

def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res

class Weapon(object):
    def __init__(self):
        self.transforms = transforms.Compose([
            Resize(),
            Normalize(),
            ToTensor()
        ])
        self.model = None

    def load(self, cdir):
        checkpoint = torch.load(cdir)
        self.num_classes = checkpoint['num_classes']
        self.class_names = checkpoint['class_names']
        self.model = resnet50(pretrained=False, num_classes=self.num_classes, download=False)
        model_dict = dict()
        state_dict = self.model.state_dict()
        for k, v in checkpoint['model_state_dict'].items():
            model_dict[k] = v
        state_dict.update(model_dict)
        self.model.load_state_dict(state_dict)
        self.model.cuda()

    def test(self, image):
        self.model.eval()
        if self.model == None:
            return "Loading or training is required."
        r'''
            image: PIL.Image in RGB
        '''
        image = self.transforms(image)
        pimage = image.unsqueeze(0).cuda()
        pred = self.model(pimage)[0]
        topk = torch.topk(pred, 5)
        res = "Model: confidence\n"
        vs = topk[0].cpu().detach().numpy()
        idxs = topk[1].cpu().detach().numpy()
        result_list = []
        for i in range(5):
            v, idx = vs[i], idxs[i]
            result_list.append(weapon_pb2.Weapon(name=self.class_names[idx], prob=v))
        return result_list
    
    def train(self, args):
        r'''args requires:
            dataset: string (dataset directory)
            epoch: int      (training epoch)
            lr: float       (learning rate)
            batch_size: int (batch size)
            saveas: string  (checkpoint save name)
        '''
        # LOAD and initialize
        train_set = Pistol(args, split='train')
        test_set = Pistol(args, split='test')
        self.train_loader = data.DataLoader(train_set, batch_size=args.batch_size, shuffle=False, num_workers=0)
        self.test_loader = data.DataLoader(test_set, batch_size=args.batch_size, shuffle=False, num_workers=0)
        self.num_classes = train_set.num_classes
        self.class_names = list()
        for i in range(self.num_classes): self.class_names.append(train_set.pred2name(i))
        self.model = resnet50(pretrained=True, num_classes=self.num_classes, download=False)
        self.model.cuda()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=args.lr)
        self.criterion = torch.nn.CrossEntropyLoss()
        
        # TRAIN
        for epoch in range(args.epoch):
            self._train(epoch)

        # SAVE
        torch.save(
            {
                'model_state_dict': self.model.state_dict(),
                'num_classes': self.num_classes,
                'class_names': self.class_names
            },
            opj('./checkpoints', args.saveas + ".pth.tar")
        )

        # VALIDATION
        with torch.no_grad():
            self._val()


    def _train(self, epoch):
        self.model.train()
        tbar = tqdm(self.train_loader)
        n = len(tbar)
        tloss = 0.0
        macc = 0.0
        for i, data in enumerate(tbar):
            self.optimizer.zero_grad()
            images = data[0].cuda()
            cats = data[1].cuda().long()

            preds = self.model(images)
            loss = self.criterion(preds, cats.squeeze(1))
            tloss += loss.item()

            tbar.set_description("(Epoch: %d) %d / %d: loss=%.4f" % (epoch, i+1, n, tloss / (i+1)))
            loss.backward()
            self.optimizer.step()

    def _val(self):
        self.model.eval()
        tbar = tqdm(self.test_loader)
        n = len(tbar)
        macc = 0.0
        top1 = AverageMeter('Acc@1', ':6.2f')
        top5 = AverageMeter('Acc@5', ':6.2f')
        for i, data in enumerate(tbar):
            images = data[0].cuda()
            cats = data[1].cuda().long()

            preds = self.model(images)

            acc1, acc5 = accuracy(preds, cats, topk=(1, 5))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))

        print("Top-1:%.3f, Top-5: %.3f" % (top1.avg, top5.avg))

if __name__ == '__main__':
    from PIL import Image
    r'''
        tests if the scripts work correctly
        1. train then test
        2. load then test
    '''
    
    r'''args requires:
        dataset: string (dataset directory)
        epoch: int      (training epoch)
        lr: float       (learning rate)
        batch_size: int (batch size)
        saveas: string  (checkpoint save name)
    '''
    class tmp():
        pass
    args = tmp()
    args.dataset = '../../datasets/pistols-40'
    args.epoch = 3
    args.lr = 1e-4
    args.batch_size = 16
    args.saveas = 'test'

    weapon = Weapon()
    image = Image.open('./samples/fnx40.png').convert('RGB')
    
    weapon.train(args)
    print(weapon.test(image))

    weapon.load('./checkpoints/test.pth.tar')
    print(weapon.test(image))
