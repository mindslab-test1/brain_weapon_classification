from models.resnet import *

import os
import torch

opj = os.path.join

if __name__ == '__main__':
    # download resnet50 pretrained checkpoint
    print("Download ResNet50 pretrained model ", end='')
    os.makedirs('./checkpoints', exist_ok=True)
    model = resnet50(pretrained=True, num_classes=1000, download=True)
    torch.save(
        {
            'model_state_dict': model.state_dict(),
            'num_classes': 0,
            'class_names': list()
        },
        "./checkpoints/resnet50_pretrained.pth.tar"
    )
    print("- Done")