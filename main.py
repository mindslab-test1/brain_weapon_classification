r'''
    Author: Sungguk Cha
    image classification script
'''

from dataloader.pistol import Pistol
from models.resnet import *
from torch.utils import data
from tqdm import tqdm

import argparse
import os
import time
import torch

opj = os.path.join

def get_args():
    parser = argparse.ArgumentParser()
    # Options
    parser.add_argument('--train', action='store_true')
    parser.add_argument('--savedir', type=str, default='./')
    parser.add_argument('--minlen', type=int)
    #parser.add_argument('--gpu-ids', type=str, default='0')

    # Test options
    parser.add_argument('--checkpoint', default=None, help='pretrained parameter dicrectory')
    parser.add_argument('--verbose', default=False, action='store_true', help='if ture, it results analysis')

    # Train options
    parser.add_argument('--model', default='resnet50')
    parser.add_argument('--dataset', type=str, help='dataset directory')
    parser.add_argument('-bs', '--batch-size', default=64, type=int)
    parser.add_argument('--epoch', type=int, default=100)
    parser.add_argument('--lr', default=1e-4)

    args = parser.parse_args()
    #args.gpu_ids = [int(s) for s in args.gpu_ids.split(',')]
    return args

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)

def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res

def train(epoch, model, optimizer, dataset):
    model.train()
    criterion = torch.nn.CrossEntropyLoss()
    tbar = tqdm(dataset)
    n = len(tbar)
    tloss = 0.0
    macc = 0.0
    for i, data in enumerate(tbar):
        optimizer.zero_grad()
        images = data[0].cuda()
        cats = data[1].cuda().long()

        preds = model(images)
        loss = criterion(preds, cats.squeeze(1))
        tloss += loss.item()

        tbar.set_description("(Epoch: %d) %d / %d: loss=%.4f" % (epoch, i+1, n, tloss / (i+1)))
        loss.backward()
        optimizer.step()

def test(model, loader, pred2name, num_classes, verbose=False):
    if verbose:
        from PIL import Image
        import numpy as np
        accuracies = list()
        for i in range(num_classes): accuracies.append([0, 0, list()])
        def recover(image):
            image = np.array(image)
            image = image.swapaxes(0, 1).swapaxes(1, 2)
            std = (0.229, 0.224, 0.225)
            mean = (0.485, 0.456, 0.406)
            image = image * std + mean
            image = image * 255
            return image.astype('uint8')
        wrongd = './wrong'
        os.makedirs(wrongd, exist_ok=True)

    model.eval()
    tbar = tqdm(loader)
    n = len(tbar)
    macc = 0.0
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    for i, data in enumerate(tbar):
        images = data[0].cuda()
        cats = data[1].cuda().long()

        preds = model(images)

        if verbose:
            for i in range(len(preds)):
                pred = preds[i].argmax().item()
                cat = data[1][i].item()
                if pred == cat:
                    accuracies[pred][0] += 1
                else:
                    accuracies[pred][1] += 1
                    image = recover(data[0][i])
                    wrong = Image.fromarray(image, 'RGB')
                    name = "{}-{}-{}.png".format(str(i), pred2name(pred), pred2name(cat))
                    saveas = opj(wrongd, name)
                    wrong.save(saveas)

        acc1, acc5 = accuracy(preds, cats, topk=(1, 5))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))

    print("Top-1:%.3f, Top-5: %.3f" % (top1.avg, top5.avg))
    if verbose:
        for i in range(num_classes):
            print("{}: {}%".format(pred2name(i), 100 * accuracies[i][0] / (accuracies[i][0] + accuracies[i][1])) )

if __name__ == '__main__':
    args = get_args()
    train_set = Pistol(args, split='train')
    test_set = Pistol(args, split='test')
    train_loader = data.DataLoader(train_set, batch_size=args.batch_size, shuffle=False, num_workers=0)
    test_loader = data.DataLoader(test_set, batch_size=args.batch_size, shuffle=False, num_workers=0)
    pred2name = train_set.pred2name
    num_classes = train_set.num_classes
    if args.model == 'resnet50':
        model = resnet50(pretrained=True, num_classes=train_set.num_classes)
    elif args.model == 'resnet101':
        model = resnet101(pretrained=True, num_classes=train_set.num_classes)
    else:
        raise NotImplementedError("{} is not supported yet".format(args.model))
    #model = torch.nn.DataParallel(model, device_ids=args.gpu_ids)
    model = model.cuda()

    if False:
        class_names = list()
        for i in range(train_set.num_classes):
            class_names.append(pred2name(i))
        os.makedirs(args.savedir, exist_ok=True)    
        torch.save(
            {
                'model_state_dict': model.state_dict(),
                'num_classes': train_set.num_classes,
                'class_names': class_names
            },
            opj(args.savedir, "resnet50.pth.tar")
        )
        exit()

    if args.checkpoint is not None:
        checkpoint = torch.load(args.checkpoint)
        model.load_state_dict(checkpoint['model_state_dict'])
    if args.train:
        optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
        for epoch in range(args.epoch):
            train(epoch, model, optimizer, train_loader)
        name = "{}-{}.pth.tar".format(args.dataset.split('/')[-1], args.model)
        os.makedirs(args.savedir, exist_ok=True)
        torch.save(
            {'model_state_dict': model.state_dict()},
            opj(args.savedir, name)
        )
    with torch.no_grad():
        test(model, test_loader, pred2name, num_classes, args.verbose)
