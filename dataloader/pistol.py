r'''
    Author: Sungguk Cha
    Pistol dataset loader
'''

from omegaconf import OmegaConf
from PIL import Image
from torch.utils import data
from torchvision import transforms

import numpy as np
import os
import random
import torch

opj = os.path.join

class Pistol(data.Dataset):
    def __init__(self, args, split):
        r'''
            args requires:
                dataset: string (dataset dir)
                minlen: int     (minimum number of images per class)
        '''
        self.split = split
        self.ddir = args.dataset
        self.categories = OmegaConf.load(opj(self.ddir, "categories.yaml"))
        self.image_set = OmegaConf.load(opj(self.ddir, "imageset.yaml"))
        self.num_classes = len(self.categories)
        self.sum = 0
        self.len_max = 0
        self.ii = list()

        if split == 'train':
            for i in range(len(self.image_set)):
                l = len(self.image_set[i])
                self.image_set[i] = self.image_set[i][:l//5*4]
                self.sum += len(self.image_set[i])
                self.ii.append(-1)
                self.len_max = max(self.len_max, len(self.image_set[i]))
        elif split == 'test':
            for i in range(len(self.image_set)):
                l = len(self.image_set[i])
                self.image_set[i] = self.image_set[i][l//5*4:]
                self.sum += len(self.image_set[i])
                self.ii.append(-1)
                self.len_max = max(self.len_max, len(self.image_set[i]))
        else:
            raise NotImplementedError("Split {} is not supported. ('train' and 'test' are supported)".format(split))

        self.composed = dict()
        self.composed['train'] = transforms.Compose([
            Resize(),
            Flip(),
            Rotate(),
            Normalize(),
            ToTensor()
        ])
        self.composed['test'] = transforms.Compose([
            Resize(),
            Normalize(),
            ToTensor()
        ])
        
        print("{} set: {} classes and {} images are loaded".format(self.split, self.num_classes, self.sum))

    def pred2name(self, pred):
        return self.categories[pred]
    
    def __getitem__(self, index):
        if self.split == 'train':
            cat = random.randint(0, self.num_classes-1)
            self.ii[cat] = (self.ii[cat] + 1) % len(self.image_set[cat])
            if self.ii[cat] == 0: random.shuffle(self.image_set[cat])
            img = self.image_set[cat][self.ii[cat]]
            img = Image.open(img).convert('RGB')
            img = self.composed[self.split](img)
            cat = torch.from_numpy(np.array([cat])).long()
            return (img, cat)
        else:
            for i in range(self.num_classes):
                if index >= len(self.image_set[i]):
                    index -= len(self.image_set[i])
                    continue
                img = self.image_set[i][index]
                img = Image.open(img).convert('RGB')
                img = self.composed[self.split](img)
                cat = torch.from_numpy(np.array([i])).long()
                return (img, i)


    def __len__(self):
        if self.split == 'train':
            return self.len_max * self.num_classes
        return self.sum

class Resize:
    def __init__(self, size=(224, 224)):
        self.size = size
    def __call__(self, image):
        image = image.resize(self.size, Image.BILINEAR)
        return image

class Flip:
    def __call__(self, image):
        if random.random() < 0.5:
            image = image.transpose(Image.FLIP_LEFT_RIGHT)
        return image

class Rotate:
    def __call__(self, image):
        rotate_degree = random.uniform(0, 360)
        image = image.rotate(rotate_degree, Image.BILINEAR)
        return image

class Normalize:
    def __call__(self, image):
        image = np.array(image).astype(np.float32)
        image /= 255.0
        image -= (0.485, 0.456, 0.406)
        image /= (0.229, 0.224, 0.225)
        return image

class ToTensor:
    def __call__(self, image):
        image = np.asarray(image).astype(np.float32).transpose((2, 0, 1))
        image = torch.from_numpy(image).float()
        return image

if __name__ == '__main__':
    class Args:
        pass
    args = Args()
    args.dataset = '../../datasets/weapons'
    args.minlen = 50
    pistol = Pistol(args, 'train')
    print(pistol.categories)
