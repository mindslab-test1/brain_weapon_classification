r'''
    Author: Sungguk Cha
    Pistol dataset loader
'''

from omegaconf import OmegaConf
from PIL import Image
from torch.utils import data
from torchvision import transforms

import numpy as np
import os
import random
import torch
import yaml

opj = os.path.join

class Pistol(data.Dataset):
    def __init__(self, args):
        r'''
            args requires:
                dataset: string (dataset dir)
                minlen: int     (minimum number of images per class)
        '''

        r'''
            Blueprint
            1. Load every available image
            2. Divide the image set into 'train' and 'test' sets, and save.
            3. Save categories as well. 
        '''

        self.ddir = args.dataset
        self.categories = list()
        self.image_set = list()
        self.ii = list()
        self.sum = 0
        self.len_max = 0
        wdirs = os.listdir(self.ddir)
        wdirs.sort()
        for wdir in wdirs:
            images = list()
            widir = opj(self.ddir, wdir)
            try:
                idir_list = os.listdir(widir)
            except:
                continue
            if len(idir_list) < args.minlen: continue
            for idir in idir_list:
                img = opj(widir, idir)
                try:
                    _ = Image.open(img).convert('RGB')
                    images.append(img)
                except:
                    continue
            if len(images) < args.minlen: continue
            self.sum += len(images)
            self.len_max = max(self.len_max, len(images))
            self.ii.append(-1)
            self.categories.append(wdir)
            self.image_set.append(images)
        self.num_classes = len(self.categories)
        print("{} classes are loaded".format(self.num_classes))
        print("{} images are loaded".format(self.sum))

        category_info = opj(self.ddir, "categories.yaml")
        imageset_info = opj(self.ddir, "imageset.yaml")
        with open(category_info, 'w') as f:
            yaml.dump(self.categories, f)
        with open(imageset_info, 'w') as f:
            yaml.dump(self.image_set, f)

        self.composed = dict()
        self.composed['train'] = transforms.Compose([
            Resize(),
            Flip(),
            Rotate(),
            Normalize(),
            ToTensor()
        ])
        self.composed['test'] = transforms.Compose([
            Resize(),
            Normalize(),
            ToTensor()
        ])

    def pred2name(self, pred):
        return self.categories[pred]
    
    def __getitem__(self, index):
        if self.split == 'train':
            cat = random.randint(0, self.num_classes-1)
            self.ii[cat] = (self.ii[cat] + 1) % len(self.image_set[cat])
            if self.ii[cat] == 0: random.shuffle(self.image_set[cat])
            img = self.image_set[cat][self.ii[cat]]
            img = Image.open(img).convert('RGB')
            img = self.composed[self.split](img)
            cat = torch.from_numpy(np.array([cat])).long()
            return (img, cat)
        else:
            for i in range(self.num_classes):
                if index >= len(self.image_set[i]):
                    index -= len(self.image_set[i])
                    continue
                img = self.image_set[cat][index]
                img = Image.open(img).convert('RGB')
                img = self.composed[self.split](img)
                cat = torch.from_numpy(np.array([cat])).long()
                return (img, cat)


    def __len__(self):
        return self.sum

class Resize:
    def __init__(self, size=(224, 224)):
        self.size = size
    def __call__(self, image):
        image = image.resize(self.size, Image.BILINEAR)
        return image

class Flip:
    def __call__(self, image):
        if random.random() < 0.5:
            image = image.transpose(Image.FLIP_LEFT_RIGHT)
        return image

class Rotate:
    def __call__(self, image):
        rotate_degree = random.uniform(0, 360)
        image = image.rotate(rotate_degree, Image.BILINEAR)
        return image

class Normalize:
    def __call__(self, image):
        image = np.array(image).astype(np.float32)
        image /= 255.0
        image -= (0.485, 0.456, 0.406)
        image /= (0.229, 0.224, 0.225)
        return image

class ToTensor:
    def __call__(self, image):
        image = np.asarray(image).astype(np.float32).transpose((2, 0, 1))
        image = torch.from_numpy(image).float()
        return image

if __name__ == '__main__':
    args = OmegaConf.load('./configs/dataset.yaml')
    pistol = Pistol(args)
    print("Dataset reloaded")
