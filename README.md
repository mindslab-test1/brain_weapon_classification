# Weapon Classification

Author: *Sungguk Cha*

This repository contains an API server that classifies an image input. This document explains the functions and the usages with examples. 

## Usages

Use following commands with appropriate modifications (e.g., 'gpus' variable in server.sh).
Please double check the dataset path in *server.sh* file. 

### Reloading dataset
Once the dataset is changed, our framework requires *reloading preprocess*. 
After checking "./configs/dataset.yaml" file, please type the following command.
```Shell
$ python reload.py
```

### Install
```shell
$ bash install.sh
```
```bash
# install.sh
version="1.0.0"
docker build -t weapon_search:$version .
```

### Server

```shell
$ bash server.sh
```

```bash
# server.sh
dataset_dir="/absolute/path/to/dataset"
gpus="0"
version="1.1.0"
external_port=46700
internal_port=33633
docker run -dit --restart on-failure --gpus '"device=$gpus"' -p $external_port:$internal_port -v $dataset_dir:/root/dataset --name weapon_container weapon_search:$version
```


### Client

The server supports following functions.
 - **Training** the classification model from a dataset.
 - **Loading** a pretrained model.
 - **Testing**, which requires an image input among the trained classes and predicts the class.

For **train**, please edit the yaml ("./configs/train.yaml") file appropriately, including the dataset path. Please note that the dataset composition should follow the format below. Naming rule is very flexible, meaning any name for the category and the images are allowed. Please do not put a non-image in the dataset.
```
Dataset_Folder
-> category1
--> image1.png
--> image2.png
...
-> category2
--> image1.png
...
```
Run
```shell
$ bash train.sh
```
```bash
# train.sh
yaml="./configs/train.yaml"
python client.py --mode="train" --yaml-dir=$yaml
```
For loading a trained model,
```shell
$ bash load.sh
```

For **load**, please run with appropriate modification.
```shell
$ bash load.sh
```
```bash
# load.sh
checkpoint="/path/to/checkpoint"
python client.py --mode="load" --checkpoint=$checkpoint
```

For **test**, 
```shell
$ bash test.sh
```
```bash
# test.sh
image_dir="/path/to/image"
python client.py --mode="test" --image-dir=$image_dir
```

### Examine
```bash
container_name="weapon_container"
docker exec -it $container_name /bin/bash
```
