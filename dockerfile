FROM pytorch/pytorch:1.6.0-cuda10.1-cudnn7-runtime

RUN apt-get update
RUN apt-get install 'ffmpeg'\
    'libsm6'\
    'libxext6'\
    vim\
    -y

RUN mkdir /root/weaponsearch
COPY . /root/weaponsearch
WORKDIR /root/weaponsearch

RUN pip install -r requirements.txt
RUN python setup.py
# RUN python reload.py
RUN python -m pip --no-cache-dir install --upgrade pip grpcio grpcio-tools && \
    ldconfig && \
    rm -rf /workspace/* && \
    python -m grpc.tools.protoc -I. --python_out . --grpc_python_out . ./weapon.proto

EXPOSE 33633

ENTRYPOINT ["python", "-u", "server.py"]
